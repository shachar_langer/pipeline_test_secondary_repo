#! /bin/bash

# -o pipefail - Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.
# -u - Attempt to use undefined variable outputs error message, and forces an exit
# -e - Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -euo pipefail

TRIGGERED_COMMIT=$(echo $BITBUCKET_COMMIT | cut -c1-6)
BRANCH_NAME="bump_submodule_to_$TRIGGERED_COMMIT"
COMMIT_MESSAGE="Bumped submodule to $TRIGGERED_COMMIT"

git clone https://$BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD@bitbucket.org/shachar_langer/pipeline_test_main_repo.git
echo cloned successfully

cd pipeline_test_main_repo
git config user.name "Bot"
git config user.email "shachar.langer@gmail.com"

git submodule update --init --remote --merge
echo updated submodule to master
git checkout -b $BRANCH_NAME
git add pipeline_test_secondary_repo
echo tracking latest submodule
git commit -m "$COMMIT_MESSAGE"
echo created new commit
git push --set-upstream origin $BRANCH_NAME
echo pushed successfully

# create a new PR
CREATE_PR_BODY='{
        "title": "'"$COMMIT_MESSAGE"'",
        "source": {
            "branch": {
                "name": "'"$BRANCH_NAME"'"
            }
        },
        "destination": {
            "branch": {
                "name": "develop"
            }
        },
        "reviewers" :[]
    }'

curl https://api.bitbucket.org/2.0/repositories/shachar_langer/pipeline_test_main_repo/pullrequests \
    -u $BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "$CREATE_PR_BODY"