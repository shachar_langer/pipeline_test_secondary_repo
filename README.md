This repository is a submodule for [this](https://bitbucket.org/shachar_langer/pipeline_test_main_repo/src/master/) repository.  Every time the `master` branch of this repository is updated, a Bitbucket Pipeline runs.
You can find the Pipeline configuration under `bitbucket-pipelines.yml`. In this configuration, you can see we're running a Bourne shell script every time the master is updated.

```yaml
pipelines:
 branches:
   master:
     - step:
         name: Bump version on main repo
         script:
           - echo "I should run only on master"
           - sh create_pr_in_main_repo.sh
```

The script `create_pr_in_main_repo.sh` updates the pointer in the main repository to reflect the latest commit and creates a PR with the change.
We do this so that we update the main repository every time we change this repository.

### Setup ###
If you want to replicate this behavior for another main repository, please change the hard-coded repository name in the script and make sure to define the BITBUCKET_USERNAME and BITBUCKET_APP_PASSWORD variables in the repository settings.